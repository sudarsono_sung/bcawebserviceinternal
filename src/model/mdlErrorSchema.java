package model;

import com.google.gson.annotations.SerializedName;

public class mdlErrorSchema {
    @SerializedName(value="error_code")
    public String ErrorCode;
    
    @SerializedName(value="error_message")
    public model.mdlMessage ErrorMessage;
}
