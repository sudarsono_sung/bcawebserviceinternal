package adapter;

import adapter.Base64Adapter;

public class ClientIdAdapter {
	public static String getAuthorizationKey(String ClientID, String ClientSecret){
		String stringToSign = ClientID + ":" + ClientSecret;
		String authorizationKey  = Base64Adapter.EncriptBase64(stringToSign); //generate apiKey
		return authorizationKey;
	}
	
	public static String getAppAuthorizationKey(String appClientID, String appClientSecret){
		String stringToSign = appClientID + ";" + appClientSecret;
		String appAuthorizationKey  = Base64Adapter.EncriptBase64(stringToSign); //generate apiKey
		return appAuthorizationKey;
	}
}
